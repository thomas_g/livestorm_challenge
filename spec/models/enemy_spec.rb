# frozen_string_literal: true

require './lib/config/dependencies'

RSpec.describe Enemy do
  subject(:enemy) { described_class.new(enemy_id, player) }

  let(:enemy_id) { 1 }
  let(:player) { Player.new }

  describe 'validations' do
    context 'when :id is nil' do
      let(:enemy_id) { nil }

      it { expect { enemy }.to raise_exception(ArgumentError, "No enemy found with the id #{enemy_id}.") }
    end

    context 'when :id is not found' do
      let(:enemy_id) { 42 }

      it { expect { enemy }.to raise_exception(ArgumentError, "No enemy found with the id #{enemy_id}.") }
    end

    it 'raises an ArgumentError when :label is nil' do
      ENEMIES[enemy_id][:label] = nil
      expect { enemy }.to raise_exception(ArgumentError, 'Label can\'t be blank.')
    end
  end

  describe 'methods' do
    let(:enemy_id) { 2 }

    describe '#attack' do
      it 'calls fight' do
        allow_any_instance_of(IoService).to receive(:call).and_return(:fight)
        expect(enemy).to receive(:fight)
        enemy.attack
      end

      it 'calls run' do
        allow_any_instance_of(IoService).to receive(:call).and_return(:run)
        expect(enemy).to receive(:run)
        enemy.attack
      end
    end

    describe '#fight' do
      context 'when player wins' do
        before do
          allow_any_instance_of(described_class).to receive(:kill_enemy?).and_return(true)
          allow_any_instance_of(IoService).to receive(:call).and_return(nil)
        end

        context 'when enemy is a boss' do
          it 'wins the game' do
            expect(Game).to receive(:exit).with(:win)
            enemy.fight
          end
        end

        context 'when enemy is not boss' do
          before { ENEMIES[enemy_id][:boss] = false }

          it 'continues the game' do
            expect(enemy.fight).to eq(:win_fight)
          end
        end
      end

      context 'when player looses' do
        it 'looses the game' do
          allow_any_instance_of(described_class).to receive(:kill_enemy?).and_return(false)
          allow_any_instance_of(IoService).to receive(:call).and_return(nil)
          expect(Game).to receive(:exit).with(:die)
          enemy.fight
        end
      end
    end

    describe '#run' do
      it 'returns run when player can escape' do
        allow_any_instance_of(described_class).to receive(:escape?).and_return(true)
        allow_any_instance_of(IoService).to receive(:call).and_return(nil)
        expect(enemy.run).to eq(:run)
      end

      it 'triggers a fight when player can not escape' do
        allow_any_instance_of(described_class).to receive(:escape?).and_return(false)
        allow_any_instance_of(IoService).to receive(:call).and_return(nil)
        expect(enemy).to receive(:fight)
        enemy.run
      end
    end
  end
end
