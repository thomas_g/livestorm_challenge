# frozen_string_literal: true

require './lib/config/dependencies'

RSpec.describe Room do
  describe 'validations' do
    context 'when :enter is empty' do
      it 'is pending'
    end

    context 'when :directions is empty' do
      it 'is pending'
    end
  end

  describe 'methods' do
    describe '#enter' do
      it 'is pending'
    end

    describe '#explore' do
      it 'is pending'
    end

    describe '#leave' do
      it 'is pending'
    end
  end
end
