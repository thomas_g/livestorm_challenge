# frozen_string_literal: true

require './lib/config/dependencies'

RSpec.describe GameCommandService do
  subject(:game_command_service) { described_class.call(cmd) }

  context 'with a valid command' do
    let(:cmd) { :help }

    it 'calls the correct method' do
      expect(Game).to receive(cmd)
      game_command_service
    end
  end

  context 'with an invalid command' do
    let(:cmd) { :invalid_random_command }

    it 'calls the correct method' do
      expect { game_command_service }.to raise_exception(ArgumentError, 'Command not available')
    end
  end
end
