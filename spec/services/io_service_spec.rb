# frozen_string_literal: true

require './lib/config/dependencies'

RSpec.describe IoService do
  subject(:io_service) { described_class.call(cmd) }

  let(:label)     { 'Some label' }
  let(:input_msg) { nil }
  let(:opt)       { {} }

  context 'without :input_msg' do
    it 'is pending'
  end

  context 'when user input empty' do
    it 'is pending'
  end

  context 'when user input invalid' do
    it 'is pending'
  end

  context 'with :valid_inputs' do
    it 'is pending'
  end

  context 'when user input is a game command' do
    it 'is pending'
  end
end
