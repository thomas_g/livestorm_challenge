# frozen_string_literal: true

class IoService
  def self.call(label, input_msg = nil, opt = {})
    new(label, input_msg, opt).call
  end

  def call
    print_label
    input_invite if @input_msg
  end

  private

  def initialize(label, input_msg, opt)
    @label = label
    @input_msg = input_msg
    @opt = opt
  end

  def print_label
    output(@label)
  end

  def input_invite
    output(@input_msg)
    while (cmd = Readline.readline('> ', true).strip.downcase)
      validate(cmd) && break
    end

    cmd
  end

  def output(str)
    printf [str, "\n"].join
  end

  def validate(cmd)
    if cmd.empty?
      output(LABELS[:io][:null])
      output(@input_msg)
      false
    elsif GAME_COMMANDS.include?(cmd.to_sym)
      GameCommandService.call(cmd.to_sym)
      output(@input_msg)
      false
    elsif @opt[:valid_inputs].nil? || @opt[:valid_inputs].include?(cmd.to_sym)
      true
    else
      output(LABELS[:io][:invalid])
      output(@input_msg)
      false
    end
  end
end
