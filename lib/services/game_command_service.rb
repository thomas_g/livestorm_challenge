# frozen_string_literal: true

class GameCommandService
  def self.call(cmd)
    new(cmd).call
  end

  def call
    Game.send(@cmd)
  end

  private

  def initialize(cmd)
    raise ArgumentError, 'Command not available' unless GAME_COMMANDS.include? cmd

    @cmd = cmd
  end
end
