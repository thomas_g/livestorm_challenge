# frozen_string_literal: true

GAME_COMMANDS = %i[help exit].freeze
GAME_ACTIONS = %i[explore leave fight run north east south west].freeze

LABELS = {
  game: {
    new_game: 'I see that you are up for an adventure. Type \'help\' at any time to display available commands.',
    start_game: 'Welcome the Dungeon of Naheulbeuk fearless ',
    exit: {
      user: 'So sad to see you chicken out.',
      die: 'Your life ends here, miserably...',
      win: 'You have free the Dungeon of Naheulbeuk from evil. Time to go celebrate at the tavern !'
    }
  },
  player: {
    ask_name_label: 'First let me introduce myself. I\'m Gudrill. I\'ve haven\'t'\
      ' seen an elf for a long time around here.',
    ask_name_input: 'What name do you go by ?'
  },
  io: {
    null: 'Please, talk to me...',
    invalid: 'I don\'t quite understand',
    prompt: 'What do you want to do ?',
    prompt_go: 'Where do you want to go ?'
  },
  room: {
    item_found: 'You found',
    leave: 'You decide to leave the room.'
  }
}.freeze

ITEMS = {
  candies: 'a Chiantos candies pack',
  sword: 'the Sword of destiny'
}.freeze

MAP = {
  1 => {
    enter: 'You\'re in the main room of the Dungeon. You can hear the fire from the big chandelier hanging above.',
    description: 'Nothing here but an empty cabinet.',
    enemy_id: nil,
    item: nil,
    directions: {
      north: 2,
      east: 3,
      west: 4
    }
  },
  2 => {
    enter: 'It must be a dinning hall. A long table is sitting in the middle and you see some meat is still smoking.',
    description: 'The table is full of food. There might be something for you to eat.',
    enemy_id: nil,
    item: :candies,
    directions: {
      south: 1
    }
  },
  3 => {
    enter: 'A smell of dwarf is all over that room, something must have happened not a long ago.',
    description: 'The locker of the chest in a corner had no chance against you lockpicking skills.',
    enemy_id: nil,
    item: :sword,
    directions: {
      west: 1
    }
  },
  4 => {
    enter: 'The light is low and you can hear metal klinging and wind blowing.',
    description: 'This is a long corridor, nothing special here.',
    enemy_id: 1,
    item: nil,
    directions: {
      east: 1,
      west: 5
    }
  },
  5 => {
    enter: 'You have to push an heavy door to finaly enter the last room.',
    enemy_id: 2,
    item: nil,
    directions: {
      east: 4
    }
  }
}.freeze

ENEMIES = {
  1 => {
    label: 'A creepy skeleton jumps on you from the ceiling. What do you do ?'
  },
  2 => {
    label: 'There he is, laying on his throne, waiting for you. What do you do ?',
    boss: true
  }
}.freeze
