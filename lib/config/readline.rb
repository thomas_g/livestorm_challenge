# frozen_string_literal: true

# Handling SIGINT
stty_save = `stty -g`.chomp
trap('INT') do
  system('stty', stty_save)
  exit
end

# Autocompletion
completion_list = GAME_ACTIONS + GAME_COMMANDS
completion_list.sort!
comp = proc { |s| completion_list.grep(/^#{Regexp.escape(s)}/) }
Readline.completion_append_character = nil
Readline.completion_proc = comp
