# frozen_string_literal: true

class Game
  def start
    IoService.call([LABELS[:game][:start_game], @player.name].join)
    loop do
      @current_room.enter
      set_room
    end
  end

  def self.exit(context = :user)
    abort(LABELS[:game][:exit][context])
  end

  def self.help
    message = <<~HEREDOC
      [##########   Help   ##########]
      Bellow are the commands that you can use during a game:
      help: display help messages
      exit: quit the game
      [##############################]
    HEREDOC
    IoService.call(message)
  end

  private

  def initialize
    IoService.call(LABELS[:game][:new_game])
    @player = Player.new
    @player.set_name
    @current_room = Room.new(1, @player)
  end

  def set_room
    @current_room = Room.new(@current_room.next_room_id, @player) if @current_room.next_room_id
  end
end
