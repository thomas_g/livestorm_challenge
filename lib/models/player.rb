# frozen_string_literal: true

class Player
  attr_accessor :name, :items

  def set_name
    set_player_name
  end

  private

  def initialize
    @items = []
  end

  def set_player_name
    @name = IoService.call(
      LABELS[:player][:ask_name_label], LABELS[:player][:ask_name_input]
    ).capitalize
  end
end
