# frozen_string_literal: true

class Enemy
  def attack
    action = IoService.call(@label, [LABELS[:io][:prompt], choices.join('/')].join(' '), valid_inputs: choices)
    public_send(action)
  end

  def fight
    if kill_enemy?
      IoService.call('Your opponent does not survive your attack.')
      Game.exit(:win) if @boss
      :win_fight
    else
      Game.exit(:die)
    end
  end

  def run
    if escape?
      :run
    else
      IoService.call('You can\'t run and have to fight...')
      fight
    end
  end

  private

  def initialize(id, player)
    @player = player
    enemy_attrs = ENEMIES[id]
    raise(ArgumentError, "No enemy found with the id #{id}.") unless enemy_attrs

    @id = id
    @label = enemy_attrs[:label] || raise(ArgumentError, 'Label can\'t be blank.')
    @boss = enemy_attrs[:boss]
  end

  def choices
    %i[fight run]
  end

  def escape?
    outcomes = [true, false]
    outcomes << true if @player.items.include?(:candies)
    outcomes.sample
  end

  def kill_enemy?
    outcomes = [true, false]
    outcomes << true if @player.items.include?(:sword)
    outcomes.sample
  end
end
