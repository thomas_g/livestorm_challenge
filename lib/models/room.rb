# frozen_string_literal: true

class Room
  attr_reader :next_room_id

  def enter
    action = encounter_enemy
    action ||= IoService.call(@enter, [LABELS[:io][:prompt], choices.join('/')].join(' '), valid_inputs: choices)
    public_send(action)
  end

  def explore
    IoService.call(@description)
    return unless @item && !@player.items.include?(@item)

    @player.items << @item
    IoService.call([LABELS[:room][:item_found], ITEMS[@item], '!'].join(' '))
  end

  def leave
    direction_values = @directions.map(&:first)
    direction = IoService.call(
      LABELS[:room][:leave],
      [LABELS[:io][:prompt_go], direction_values.join('/')].join(' '),
      valid_inputs: direction_values
    )
    @next_room_id = @directions[direction.to_sym]
    IoService.call('*' * 30)
  end

  private

  def initialize(id, player)
    @player = player
    room_attrs = MAP[id]
    @id = id
    @enter = room_attrs[:enter] || raise(ArgumentError, 'Enter message can\'t be blank.')
    @description = room_attrs[:description]
    @enemy_id = room_attrs[:enemy_id]
    @item = room_attrs[:item]
    @directions = room_attrs[:directions] || raise(ArgumentError, 'Directions can\'t be blank.')
  end

  def choices
    %i[explore leave]
  end

  def encounter_enemy
    return unless @enemy_id

    IoService.call(@enter)
    encounter = Enemy.new(@enemy_id, @player).attack
    @enemy_id = nil if encounter == :win_fight
    encounter == :run ? :leave : nil
  end
end
