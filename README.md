# Livestorm Challenge

This is a little rpg playable through REPL.

The specs of the challenge are available here: https://gist.github.com/Loschcode/4871bbc0643f75c9dd9558ac5ebf6014

The code runs on ruby 2.7.1. If you are using RVM, it will create a new gemset named livestorm-challenge when you enter the directory of the project.
The gems contained in the gemfile only provide tools for developpers and are not necessary to execute the code.

To launch the game, run `ruby game.rb`.

To setup the gems, install bundler with `$ gem install bundler` then you can run `bundle install`.

### Rspec

To launch the test suite, run `rspec`.
